package pl.sdacademy.database.daoimpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sdacademy.database.dao.NfcTagDao;
import pl.sdacademy.database.entity.NfcTag;
import pl.sdacademy.database.utils.HibernateUtils;

import static org.junit.jupiter.api.Assertions.*;

class NfcTagDaoImplTest {

    private NfcTagDao tagDao = new NfcTagDaoImpl();

    @BeforeEach
    private void clearTable() {
        SessionFactory factory = HibernateUtils
                .getInstance()
                .getSessionFactory();
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        session
                .createQuery("delete NfcTag ")
                .executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    @Test
    void save() {
        NfcTag tag = new NfcTag();
        tag.setSerialNumber("serial serial serial");

        tagDao.save(tag);

        NfcTag saved = tagDao.findById(tag.getId());

        assertNotNull(saved);
    }
}