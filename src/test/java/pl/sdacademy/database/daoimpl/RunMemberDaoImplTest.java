package pl.sdacademy.database.daoimpl;

import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sdacademy.database.dao.RunMemberDao;
import pl.sdacademy.database.entity.RunMember;
import pl.sdacademy.database.utils.HibernateUtils;

import static org.junit.jupiter.api.Assertions.*;

class RunMemberDaoImplTest {

    private RunMemberDao runMemberDao = new RunMemberDaoImpl();

    @BeforeEach
    void clearTable() {
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();
        session.createQuery("delete RunMember").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    @Test
    void save() {
        RunMember member = new RunMember();
        member.setName("Damian");

        runMemberDao.save(member);

        RunMember saved = runMemberDao.findById(member.getId());

        assertNotNull(saved);
    }

    @Test
    void findByNameFragment() {
        RunMember member1 = new RunMember();
        member1.setName("Damian");

        RunMember member2 = new RunMember();
        member2.setName("Woien");

        runMemberDao.save(member1);
        runMemberDao.save(member2);

        assertEquals(2, runMemberDao.findByNameFragment("%i_n").size());
        assertEquals(0, runMemberDao.findByNameFragment("olek").size());
    }
}